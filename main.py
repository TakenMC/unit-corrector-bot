import os
from datetime import datetime

import discord
from discord.ext import commands

from libs import filter, unitconversion

status = discord.Status.dnd
activity = discord.Activity(
    name='"freedom" units autocorrect', type=discord.ActivityType.playing
)

intents = discord.Intents.default()
intents.message_content = True
intents.members = True
intents.guilds = True
intents.guild_messages = True
intents.messages = True

description = """UnitCorrector: A community-beveloped open source Discord bot that corrects non-SI units to SI ones! Also features a !unitpedia command, allowing users to learn about (all) units."""
bot = commands.Bot(
    command_prefix="uc!",
    description=description,
    intents=intents,
    status=status,
    activity=activity,
)

starttime = datetime.now()
longprefix = ":symbols: UnitCorrector | "
shortprefix = ":symbols: "


@bot.event
async def on_ready():
    if bot.user:
        print(
            "Discord Unit Corrector Bot: Logged in as {} (id: {})\n".format(
                bot.user.name, bot.user.id
            )
        )
    else:
        print("Discord Unit Corrector Bot: Failed to log in.")


@bot.event
async def on_message(message: discord.Message):
    if (
        bot.user
        and bot.user.id is not message.author.id
        and message.author.bot is False
        # and (
        #     message.guild is None
        #     or (
        #         message.guild is not None
        #         and discord.utils.get(message.guild.roles, name="imperial certified")
        #         not in message.author.roles
        #     )
        # )
    ):
        processedMessage = unitconversion.process(message.content)
        if processedMessage is not None:
            correctionText = (
                "I think "
                + filter.apply_strict(
                    message.author.display_name if message.guild is not None else "you"
                )
                + " meant to say: ```"
                + filter.apply_strict(processedMessage)
                + "```"
            )
            await message.channel.send(correctionText)
    await bot.process_commands(message)


@bot.event
async def on_command(ctx):
    print("[{}] Fired {} by {}".format(datetime.now(), ctx.command, ctx.author))


@bot.command(name="uptime", hidden=True)
async def uptime(ctx):
    await ctx.send(
        shortprefix
        + "Uptime\n```Bot started: {}\nBot uptime: {}```".format(
            starttime, (datetime.now() - starttime)
        )
    )


if token := os.environ.get("TOKEN"):
    bot.run(token)
else:
    print("No token found, please set the DCBTOKEN environment variable.")
